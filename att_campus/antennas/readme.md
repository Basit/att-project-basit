# Antennas 

## NNH4 sectored antenna 
### The positive 45 degree pattern
![Pattern NNH4](../images/Patterns_NNH4-65C-R6-V3_Port_6_+45_2355_MHz.png)

### The negative 45 degree pattern
![Pattern NNH4](../images/Patterns_NNH4-65C-R6-V3_Port_6_-45_2355_MHz.png)

[Link to antenna](https://www.commscope.com/product-type/antennas/base-station-antennas-equipment/base-station-antennas/itemnnh4-65c-r6-v3)


## SBNHH sectored antenna 
### The positive 45 degree pattern
![Pattern SBNHH](../images/Horizontal_Pattern_SBNHH-1D65B_Port_3_+45_2355_MHz.png)

### The negative 45 degree pattern.
Interesting note here is that feko would not scale this correctly, but I suspect the pattern would look very similar as the above if scaled appropriately. 


![Pattern SBNHH](../images/Horizontal_Pattern_SBNHH-1D65B_Port_4_-45_2355_MHz.png)

[Link to antenna](https://www.commscope.com/product-type/antennas/base-station-antennas-equipment/base-station-antennas/itemsbnhh-1d65b/)
